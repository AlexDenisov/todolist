//
//  AppDelegate.m
//  UI
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import "AppDelegate.h"
#import "TLAppController.h"

@interface AppDelegate ()

@property (nonatomic, strong) TLAppController *appController;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.appController = [[TLAppController alloc] initWithMainWindow:self.window];
    [self.appController showHomeScreen];
    return YES;
}

@end
