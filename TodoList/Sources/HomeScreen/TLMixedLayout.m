//
//  TLMixedLayout.m
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import "TLMixedLayout.h"

#import "TLTodoItemCell.h"
#import "TLTodoItemCellDelegate.h"
#import "TLAddTodoItemCell.h"
#import "TLEditTodoItemCell.h"
#import "TLEditTodoItemCellDelegate.h"
#import "TLCollectionViewFlowLayout.h"
#import "TLCellFactory.h"
#import "TLTodoItem.h"
#import "TLCollectionViewDragging.h"
#import "TLDataController.h"

typedef enum : NSUInteger {
    TLMixedLayoutSectionShow,
    TLMixedLayoutSectionEdit
} TLMixedLayoutSection;

@interface TLMixedLayout ()
    <UICollectionViewDataSource,
    UICollectionViewDelegate,
    UICollectionViewDelegateFlowLayout,
    TLEditTodoItemCellDelegate,
    TLTodoItemCellDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) TLCellFactory *cellFactory;

@property (nonatomic, strong) TLDataController *dataController;
@property (nonatomic, strong) NSMutableArray *todoItemsToEdit;

@property (nonatomic, strong) TLCollectionViewDragging *dragging;

@end

@implementation TLMixedLayout

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView
                        dataController:(TLDataController *)dataController {
    self = [super init];

    self.collectionView = collectionView;
    self.cellFactory = [[TLCellFactory alloc] initWithCollectionView:self.collectionView];
    self.dataController = dataController;
    self.todoItemsToEdit = [NSMutableArray new];

    self.dragging = [[TLCollectionViewDragging alloc] initWithCollectionView:self.collectionView];

    return self;
}

- (void)invalidateLayout {
    [self.collectionView reloadData];
}

- (void)prepareLayout {
    [self specifyEditTodoItemSection];
    [self assignDataSourceAndDelegate];
}

- (void)specifyEditTodoItemSection {
    TLCollectionViewFlowLayout *layout = (TLCollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    layout.editTodoItemSection = TLMixedLayoutSectionEdit;
}

- (void)assignDataSourceAndDelegate {
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
}

#pragma mark - Predicates

- (BOOL)isAddTodoItemIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == TLMixedLayoutSectionEdit) {
        return NO;
    }

    if (self.dataController.numberOfTodoItems == indexPath.item) {
        return YES;
    }

    return NO;
}

- (BOOL)isEditTodoItemIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == TLMixedLayoutSectionEdit) {
        return YES;
    }

    return NO;
}

#pragma mark - UICollectionView DataSource/Delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    // section for showing the list of items +
    // section for editing an item
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    if (section == TLMixedLayoutSectionShow) {
        // '+ 1' for 'AddTodoItemCell'
        return self.dataController.numberOfTodoItems + 1;
    } else {
        // if we have smth to edit/create - show extra cell on top
        return self.todoItemsToEdit.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == TLMixedLayoutSectionEdit) {
        return [self editTodoItemCellAtIndexPath:indexPath];
    }

    if ([self isAddTodoItemIndexPath:indexPath]) {
        return [self addTodoItemCellAtIndexPath:indexPath];
    } else {
        return [self todoItemCellAtIndexPath:indexPath];
    }
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isEditTodoItemIndexPath:indexPath]) {
        [self dismissEditSection];
    } else if ([self isAddTodoItemIndexPath:indexPath]) {
        [self presentEditSectionWithTodoItem:[TLTodoItem new]];
    } else {
        [self presentEditSectionWithTodoItem:[self.dataController todoItemAtIndex:indexPath.item]];
    }
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isAddTodoItemIndexPath:indexPath]) {
        return NO;
    }

    if ([self isEditTodoItemIndexPath:indexPath]) {
        return NO;
    }

    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView
   moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath
           toIndexPath:(NSIndexPath *)destinationIndexPath {
    [self.dataController moveTodoItemAtIndex:sourceIndexPath.item toIndex:destinationIndexPath.item];
}

#pragma mark - UICollectionViewCells

- (TLTodoItemCell *)todoItemCellAtIndexPath:(NSIndexPath *)indexPath {
    TLTodoItemCell *cell = [self.cellFactory todoItemCellAtIndexPath:indexPath];
    cell.delegate = self;
    [cell updateCellWithTodoItem:[self.dataController todoItemAtIndex:indexPath.item]];
    return cell;
}

- (TLAddTodoItemCell *)addTodoItemCellAtIndexPath:(NSIndexPath *)indexPath {
    TLAddTodoItemCell *cell = [self.cellFactory addTodoItemCellAtIndexPath:indexPath];
    return cell;
}

- (TLEditTodoItemCell *)editTodoItemCellAtIndexPath:(NSIndexPath *)indexPath {
    TLEditTodoItemCell *cell = [self.cellFactory editTodoItemCellAtIndexPath:indexPath];
    cell.editableTodoItem = self.todoItemsToEdit[indexPath.item];
    cell.delegate = self;
    return cell;
}

#pragma mark - TodoItemCell Delegate

- (void)todoItemCell:(TLTodoItemCell *)cell didChangeDoneStateOfTodoItem:(TLTodoItem *)todoItem {
    [self updateTodoItem:todoItem];
}

- (void)todoItemCell:(TLTodoItemCell *)cell didConfirmEditingOfTodoItem:(TLTodoItem *)todoItem {
    [self presentEditSectionWithTodoItem:todoItem];
}

#pragma mark - EditTodoItemCell Delegate

- (void)editTodoItemCell:(TLEditTodoItemCell *)cell didConfirmEditingOfTodoItem:(TLTodoItem *)todoItem {
    if (todoItem.isNew) {
        [self createNewTodoItem:todoItem];
    } else {
        [self updateTodoItem:todoItem];
    }
}

- (void)editTodoItemCell:(TLEditTodoItemCell *)cell didConfirmDeletingOfTodoItem:(TLTodoItem *)todoItem {
    [self deleteTodoItem:todoItem];
}

- (void)editTodoItemCellDidCancelEditing:(TLEditTodoItemCell *)cell {
    [self dismissEditSection];
}

#pragma mark - DataController/CollectionView updates

- (void)createNewTodoItem:(TLTodoItem *)newTodoItem {
    [self.collectionView performBatchUpdates:^{
        [self dismissEditSection];
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:self.dataController.numberOfTodoItems
                                                     inSection:TLMixedLayoutSectionShow];
        [self.collectionView insertItemsAtIndexPaths:@[ indexPath ]];
        [self.dataController addTodoItem:newTodoItem];
    } completion:nil];
}

- (void)updateTodoItem:(TLTodoItem *)todoItem {
    [self.collectionView performBatchUpdates:^{
        [self dismissEditSection];
        [self.dataController updateTodoItem:todoItem];
        NSUInteger index = [self.dataController.todoItems indexOfObject:todoItem];
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:TLMixedLayoutSectionShow];
        [self.collectionView reloadItemsAtIndexPaths:@[ indexPath ]];
    } completion:nil];
}

- (void)deleteTodoItem:(TLTodoItem *)todoItem {
    [self.collectionView performBatchUpdates:^{
        [self dismissEditSection];
        NSUInteger index = [self.dataController.todoItems indexOfObject:todoItem];
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:TLMixedLayoutSectionShow];
        [self.collectionView deleteItemsAtIndexPaths:@[ indexPath ]];
        [self.dataController deleteTodoItem:todoItem];
    } completion:nil];
}

#pragma mark - CollectionView management

- (void)presentEditSectionWithTodoItem:(TLTodoItem *)todoItem {
    [self.todoItemsToEdit addObject:todoItem];
    [self reloadEditSection];
}

- (void)dismissEditSection {
    [self.todoItemsToEdit removeAllObjects];
    [self reloadEditSection];
}

- (void)reloadEditSection {
    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:TLMixedLayoutSectionEdit]];
}

@end
