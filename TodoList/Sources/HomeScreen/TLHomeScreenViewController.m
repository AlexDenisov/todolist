//
//  HomeScreenViewController.m
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import "TLHomeScreenViewController_Internal.h"
#import "TLHomeScreenViewLoader.h"
#import "TLTodoItem.h"
#import "TLPalette.h"

@implementation TLHomeScreenViewController

- (void)loadView {
    [super loadView];
    TLHomeScreenViewLoader *loader = [[TLHomeScreenViewLoader alloc] initWithViewController:self];
    [loader loadView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    TLPalette *palette = [TLPalette new];
    self.navigationController.navigationBar.backgroundColor = palette.blue;
    [self switchToMixedLayout];
}

- (void)switchToMixedLayout {
    self.mixedLayout = [[TLMixedLayout alloc] initWithCollectionView:self.collectionView
                                                      dataController:self.dataController];
    [self.mixedLayout prepareLayout];
    [self.mixedLayout invalidateLayout];
}

- (void)willTransitionToTraitCollection:(UITraitCollection *)newCollection withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [self.collectionView.collectionViewLayout invalidateLayout];
}

@end
