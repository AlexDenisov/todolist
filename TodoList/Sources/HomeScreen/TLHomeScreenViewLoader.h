//
//  TLHomeScreenViewLoader.h
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import <Foundation/Foundation.h>

@class TLHomeScreenViewController;

@interface TLHomeScreenViewLoader : NSObject

- (instancetype)initWithViewController:(TLHomeScreenViewController *)viewController;

- (void)loadView;

@end
