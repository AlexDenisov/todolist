//
//  HomeScreenViewController.h
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import <UIKit/UIKit.h>

@class TLDataController;

@interface TLHomeScreenViewController : UIViewController

@property (nonatomic, strong) TLDataController *dataController;

@end
