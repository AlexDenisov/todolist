//
//  TLMixedLayout.h
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import <UIKit/UIKit.h>

@class TLDataController;

@interface TLMixedLayout : NSObject

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView
                        dataController:(TLDataController *)dataController;

- (void)prepareLayout;
- (void)invalidateLayout;

@end
