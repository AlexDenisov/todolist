//
//  TLHomeScreenViewController_Internal.h
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import "TLHomeScreenViewController.h"
#import "TLMixedLayout.h"

@interface TLHomeScreenViewController ()

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) TLMixedLayout *mixedLayout;

@end
