//
//  TLHomeScreenViewLoader.m
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import "TLHomeScreenViewLoader.h"
#import "TLTodoItemCell.h"
#import "TLHomeScreenViewController_Internal.h"
#import "TLCollectionViewFlowLayout.h"
#import "TLPalette.h"

@interface TLHomeScreenViewLoader ()

@property (nonatomic, weak) TLHomeScreenViewController *viewController;

@end

@implementation TLHomeScreenViewLoader

- (instancetype)initWithViewController:(TLHomeScreenViewController *)viewController {
    self = [super init];

    self.viewController = viewController;

    return self;
}

- (void)loadView {
    TLPalette *palette = [TLPalette new];
    self.viewController.view.backgroundColor = palette.gray;
    [self loadCollectionView];
}

#pragma mark - CollectionView

- (void)loadCollectionView {
    TLCollectionViewFlowLayout *layout = [[TLCollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.itemSize = CGSizeMake(156, 156);
    layout.minimumInteritemSpacing = 0;
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                                          collectionViewLayout:layout];
    collectionView.alwaysBounceVertical = YES;
    collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    collectionView.backgroundColor = [UIColor clearColor];
    collectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;

    NSArray *constraints = @[
                             [NSLayoutConstraint constraintWithItem:collectionView
                                                          attribute:NSLayoutAttributeBottom
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.viewController.view
                                                          attribute:NSLayoutAttributeBottom
                                                         multiplier:1
                                                           constant:0],
                             [NSLayoutConstraint constraintWithItem:collectionView
                                                          attribute:NSLayoutAttributeTop
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.viewController.view
                                                          attribute:NSLayoutAttributeTop
                                                         multiplier:1
                                                           constant:0],
                             [NSLayoutConstraint constraintWithItem:collectionView
                                                          attribute:NSLayoutAttributeLeading
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.viewController.view
                                                          attribute:NSLayoutAttributeLeading
                                                         multiplier:1
                                                           constant:0],
                             [NSLayoutConstraint constraintWithItem:collectionView
                                                          attribute:NSLayoutAttributeTrailing
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.viewController.view
                                                          attribute:NSLayoutAttributeTrailing
                                                         multiplier:1
                                                           constant:0]
                             ];

    self.viewController.collectionView = collectionView;
    [self.viewController.view addSubview:collectionView];
    [self.viewController.view addConstraints:constraints];
}

@end
