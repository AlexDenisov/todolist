//
//  TLPalette.m
//  TodoList
//
//  Created by AlexDenisov on 27/09/15.
//
//

#import "TLPalette.h"

// http://www.colourlovers.com/palette/148712/Gamebookers

static UIColor *rgb_color(int r, int g, int b) {
    return [UIColor colorWithRed:r/255.f
                           green:g/255.f
                            blue:b/255.f
                           alpha:1];
}

static UIColor *orange() {
    return rgb_color(255, 153, 0);
}

static UIColor *black() {
    return rgb_color(66, 66, 66);
}

static UIColor *white() {
    return rgb_color(233, 233, 233);
}

static UIColor *gray() {
    return rgb_color(188, 188, 188);
}

static UIColor *blue() {
    return rgb_color(50, 153, 187);
}

@implementation TLPalette

- (UIColor *)white {
    return white();
}

- (UIColor *)black {
    return black();
}

- (UIColor *)gray {
    return gray();
}

- (UIColor *)orange {
    return orange();
}

- (UIColor *)blue {
    return blue();
}

@end
