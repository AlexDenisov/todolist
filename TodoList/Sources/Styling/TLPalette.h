//
//  TLPalette.h
//  TodoList
//
//  Created by AlexDenisov on 27/09/15.
//
//

#import <UIKit/UIKit.h>

@interface TLPalette : NSObject

- (UIColor *)white;
- (UIColor *)black;
- (UIColor *)gray;
- (UIColor *)orange;
- (UIColor *)blue;

@end
