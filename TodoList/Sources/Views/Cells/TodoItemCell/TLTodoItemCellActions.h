//
//  TLTodoItemCellActions.h
//  TodoList
//
//  Created by AlexDenisov on 27/09/15.
//
//

#import <Foundation/Foundation.h>

@class TLTodoItemCell;
@protocol TLTodoItemCellDelegate;

@interface TLTodoItemCellActions : NSObject

@property (nonatomic, weak) id<TLTodoItemCellDelegate> delegate;

- (instancetype)initWithCell:(TLTodoItemCell *)cell;

@end
