//
//  TLTodoItemCellDelegate.h
//  TodoList
//
//  Created by AlexDenisov on 27/09/15.
//
//

#import <Foundation/Foundation.h>

@class TLTodoItem;
@class TLTodoItemCell;

@protocol TLTodoItemCellDelegate
    <NSObject>

- (void)todoItemCell:(TLTodoItemCell *)cell didConfirmEditingOfTodoItem:(TLTodoItem *)todoItem;
- (void)todoItemCell:(TLTodoItemCell *)cell didChangeDoneStateOfTodoItem:(TLTodoItem *)todoItem;

@end
