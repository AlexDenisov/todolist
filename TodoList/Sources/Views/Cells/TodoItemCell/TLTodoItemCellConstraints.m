//
//  TLTodoItemCellConstraints.m
//  TodoList
//
//  Created by AlexDenisov on 26/09/15.
//
//

#import "TLTodoItemCellConstraints.h"
#import "TLTodoItemCell_Internal.h"

const static CGFloat kTLButtonWidth = 36;
const static CGFloat kTLButtonHeight = 36;
const static CGFloat kTLLabelInset = 8.f;

@interface TLTodoItemCellConstraints ()

@property (nonatomic, weak) TLTodoItemCell *cell;

@property (nonatomic, strong) NSLayoutConstraint *todoItemBodyLabelBottom;
@property (nonatomic, strong) NSLayoutConstraint *checkboxButtonHeight;
@property (nonatomic, strong) NSLayoutConstraint *checkboxButtonWidth;
@property (nonatomic, strong) NSLayoutConstraint *editButtonTrailing;

@end

@implementation TLTodoItemCellConstraints

- (instancetype)initWithCell:(TLTodoItemCell *)cell {
    self = [super init];

    self.cell = cell;

    [self initReorderButtonConstraints];
    [self initEditButtonConstraints];
    [self initCheckboxButtonConstraints];
    [self initTodoItemBodyLabelConstraints];

    [self normalLayout];

    return self;
}

- (void)normalLayout {
    self.todoItemBodyLabelBottom.constant = -self.checkboxButtonHeight.constant;
    self.editButtonTrailing.constant = -self.checkboxButtonWidth.constant;
}

- (void)initReorderButtonConstraints {
    self.cell.reorderButton.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.cell.reorderButton
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1
                                                               constant:kTLButtonHeight];
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.cell.reorderButton
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1
                                                              constant:kTLButtonWidth];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.cell.reorderButton
                                                               attribute:NSLayoutAttributeLeading
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.cell
                                                               attribute:NSLayoutAttributeLeading
                                                              multiplier:1
                                                                constant:0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.cell.reorderButton
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cell
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1
                                                               constant:0];

    [self.cell addConstraints:@[
                                height,
                                width,
                                leading,
                                bottom
                                ]];
}

- (void)initEditButtonConstraints {
    self.cell.editButton.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.cell.editButton
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1
                                                               constant:kTLButtonHeight];
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.cell.editButton
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1
                                                              constant:kTLButtonWidth];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.cell.editButton
                                                                attribute:NSLayoutAttributeTrailing
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.cell
                                                                attribute:NSLayoutAttributeTrailing
                                                               multiplier:1
                                                                 constant:0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.cell.editButton
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cell
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1
                                                               constant:0];

    [self.cell addConstraints:@[
                                height,
                                width,
                                trailing,
                                bottom
                                ]];
    self.editButtonTrailing = trailing;
}

- (void)initCheckboxButtonConstraints {
    self.cell.checkboxButton.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.cell.checkboxButton
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1
                                                               constant:kTLButtonHeight];
    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.cell.checkboxButton
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1
                                                              constant:kTLButtonWidth];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.cell.checkboxButton
                                                                attribute:NSLayoutAttributeTrailing
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.cell
                                                                attribute:NSLayoutAttributeTrailing
                                                               multiplier:1
                                                                 constant:0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.cell.checkboxButton
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cell
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1
                                                               constant:0];

    [self.cell addConstraints:@[
                                height,
                                width,
                                trailing,
                                bottom
                                ]];
    self.checkboxButtonWidth = width;
    self.checkboxButtonHeight = height;
}

- (void)initTodoItemBodyLabelConstraints {
    self.cell.todoItemBodyLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.cell.todoItemBodyLabel
                                                           attribute:NSLayoutAttributeTop
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:self.cell
                                                           attribute:NSLayoutAttributeTop
                                                          multiplier:1
                                                            constant:kTLLabelInset];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.cell.todoItemBodyLabel
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cell
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1
                                                               constant:0];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.cell.todoItemBodyLabel
                                                               attribute:NSLayoutAttributeLeading
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.cell
                                                               attribute:NSLayoutAttributeLeading
                                                              multiplier:1
                                                                constant:kTLLabelInset];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.cell.todoItemBodyLabel
                                                                attribute:NSLayoutAttributeTrailing
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.cell
                                                                attribute:NSLayoutAttributeTrailing
                                                               multiplier:1
                                                                 constant:-kTLLabelInset];

    [self.cell addConstraints:@[
                                top,
                                bottom,
                                leading,
                                trailing
                                ]];
    self.todoItemBodyLabelBottom = bottom;
}

@end
