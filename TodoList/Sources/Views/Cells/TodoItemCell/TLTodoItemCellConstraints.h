//
//  TLTodoItemCellConstraints.h
//  TodoList
//
//  Created by AlexDenisov on 26/09/15.
//
//

#import <Foundation/Foundation.h>

@class TLTodoItemCell;

@interface TLTodoItemCellConstraints : NSObject

- (instancetype)initWithCell:(TLTodoItemCell *)cell;

@end
