//
//  TLTodoItemCellStyle.h
//  TodoList
//
//  Created by AlexDenisov on 27/09/15.
//
//

#import <Foundation/Foundation.h>

@class TLTodoItemCell;

@interface TLTodoItemCellStyle : NSObject

- (instancetype)initWithCell:(TLTodoItemCell *)cell;

- (void)toBeDoneStyle;
- (void)doneStyle;

@end
