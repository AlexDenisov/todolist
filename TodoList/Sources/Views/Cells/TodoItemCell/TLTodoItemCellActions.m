//
//  TLTodoItemCellActions.m
//  TodoList
//
//  Created by AlexDenisov on 27/09/15.
//
//

#import "TLTodoItemCellActions.h"
#import "TLTodoItemCell_Internal.h"
#import "TLTodoItemCellDelegate.h"
#import "TLTodoItem.h"

@interface TLTodoItemCellActions ()

@property (nonatomic, weak) TLTodoItemCell *cell;

@end

@implementation TLTodoItemCellActions

- (instancetype)initWithCell:(TLTodoItemCell *)cell {
    self = [super init];

    self.cell = cell;
    [self addTargetsAndActions];

    return self;
}

- (void)addTargetsAndActions {
    [self.cell.editButton addTarget:self
                             action:@selector(editButtonTapped:)
                   forControlEvents:UIControlEventTouchUpInside];

    [self.cell.checkboxButton addTarget:self
                                 action:@selector(checkboxButtonTapped:)
                       forControlEvents:UIControlEventTouchUpInside];
}

- (void)editButtonTapped:(UIButton *)confirmButton {
    [self.delegate todoItemCell:self.cell
    didConfirmEditingOfTodoItem:self.cell.todoItem];
}

- (void)checkboxButtonTapped:(UIButton *)cancelButton {
    BOOL flippedIsDone = !self.cell.todoItem.isDone;
    self.cell.todoItem.done = flippedIsDone;
    [self.delegate todoItemCell:self.cell
   didChangeDoneStateOfTodoItem:self.cell.todoItem];
}

@end
