//
//  TLTodoItemCell_Internal.h
//  TodoList
//
//  Created by AlexDenisov on 26/09/15.
//
//

#import "TLTodoItemCell.h"

@class TLTodoItemCellSubviews;
@class TLTodoItemCellConstraints;
@class TLTodoItemCellStyle;
@class TLTodoItemCellActions;

@interface TLTodoItemCell ()

@property (nonatomic, strong) UILabel *todoItemBodyLabel;
@property (nonatomic, strong) UIButton *reorderButton;
@property (nonatomic, strong) UIButton *editButton;
@property (nonatomic, strong) UIButton *checkboxButton;

@property (nonatomic, strong) TLTodoItemCellSubviews *cellSubviews;
@property (nonatomic, strong) TLTodoItemCellConstraints *cellConstraints;
@property (nonatomic, strong) TLTodoItemCellStyle *style;
@property (nonatomic, strong) TLTodoItemCellActions *actions;

@property (nonatomic, strong) TLTodoItem *todoItem;

@end
