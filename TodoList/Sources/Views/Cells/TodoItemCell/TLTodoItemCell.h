//
//  TLTodoItemCell.h
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import <UIKit/UIKit.h>

@class TLTodoItem;
@protocol TLTodoItemCellDelegate;

@interface TLTodoItemCell : UICollectionViewCell

@property (nonatomic, weak) id<TLTodoItemCellDelegate> delegate;

- (void)updateCellWithTodoItem:(TLTodoItem *)todoItem;
- (BOOL)isDraggableAtLocation:(CGPoint)location;

@end
