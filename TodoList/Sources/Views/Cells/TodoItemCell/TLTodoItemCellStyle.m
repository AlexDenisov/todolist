//
//  TLTodoItemCellStyle.m
//  TodoList
//
//  Created by AlexDenisov on 27/09/15.
//
//

#import "TLTodoItemCellStyle.h"
#import "TLTodoItemCell_Internal.h"
#import "TLPalette.h"

#import <CoreGraphics/CoreGraphics.h>

const static CGFloat kTLImageInset = 8.f;

@interface TLTodoItemCellStyle ()

@property (nonatomic, weak) TLTodoItemCell *cell;
@property (nonatomic, strong) TLPalette *palette;

@end

@implementation TLTodoItemCellStyle

- (instancetype)initWithCell:(TLTodoItemCell *)cell {
    self = [super init];

    self.cell = cell;
    self.palette = [TLPalette new];
    [self applyStyles];

    return self;
}

- (void)applyStyles {
    CALayer *layer = self.cell.layer;
    layer.masksToBounds = NO;
    layer.shadowOpacity = 0.75;
    layer.shadowRadius = 3;
    layer.shadowOffset = CGSizeZero;
    layer.shadowPath = [UIBezierPath bezierPathWithRect:self.cell.bounds].CGPath;
    layer.shouldRasterize = YES;
    layer.shadowColor = self.palette.blue.CGColor;

    self.cell.backgroundColor = self.palette.black;
    self.cell.todoItemBodyLabel.textColor = self.palette.white;
    self.cell.todoItemBodyLabel.font = [UIFont systemFontOfSize:22];

    [self setImageWithName:@"reorder_icon" forButton:self.cell.reorderButton];
    [self setImageWithName:@"edit_icon" forButton:self.cell.editButton];
    [self setImageWithName:@"checkbox_icon" forButton:self.cell.checkboxButton];

    self.cell.reorderButton.tintColor = self.palette.gray;
    self.cell.editButton.tintColor = self.palette.gray;
}

- (void)setImageWithName:(NSString *)imageName forButton:(UIButton *)button {
    UIEdgeInsets insets = UIEdgeInsetsMake(kTLImageInset, kTLImageInset, kTLImageInset, kTLImageInset);
    UIImage *image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [button setImage:image forState:UIControlStateNormal];
    [button setImageEdgeInsets:insets];
}

- (void)toBeDoneStyle {
    self.cell.checkboxButton.tintColor = self.palette.gray;
}

- (void)doneStyle {
    self.cell.checkboxButton.tintColor = self.palette.orange;
}

@end
