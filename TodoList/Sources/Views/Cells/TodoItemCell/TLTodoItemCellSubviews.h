//
//  TLTodoItemCellSubviews.h
//  TodoList
//
//  Created by AlexDenisov on 26/09/15.
//
//

#import <Foundation/Foundation.h>

@class TLTodoItemCell;

@interface TLTodoItemCellSubviews : NSObject

- (instancetype)initWithCell:(TLTodoItemCell *)cell;

@end
