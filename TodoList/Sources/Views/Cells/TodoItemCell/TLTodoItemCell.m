//
//  TLTodoItemCell.m
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import "TLTodoItemCell_Internal.h"
#import "TLTodoItemCellActions.h"
#import "TLTodoItemCellSubviews.h"
#import "TLTodoItemCellConstraints.h"
#import "TLTodoItemCellStyle.h"
#import "TLTodoItem.h"

#import "TLTodoItemCellDelegate.h"

@implementation TLTodoItemCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];

    self.cellSubviews = [[TLTodoItemCellSubviews alloc] initWithCell:self];
    self.cellConstraints = [[TLTodoItemCellConstraints alloc] initWithCell:self];
    self.style = [[TLTodoItemCellStyle alloc] initWithCell:self];
    self.actions = [[TLTodoItemCellActions alloc] initWithCell:self];

    return self;
}

- (void)updateCellWithTodoItem:(TLTodoItem *)todoItem {
    self.todoItem = todoItem;
    self.todoItemBodyLabel.text = todoItem.body;
    if (todoItem.isDone) {
        [self.style doneStyle];
    } else {
        [self.style toBeDoneStyle];
    }
}

- (BOOL)isDraggableAtLocation:(CGPoint)location {
    return CGRectContainsPoint(self.reorderButton.frame, location);
}

- (void)setDelegate:(id<TLTodoItemCellDelegate>)delegate {
    self.actions.delegate = delegate;
}

- (id<TLTodoItemCellDelegate>)delegate {
    return self.actions.delegate;
}

@end
