//
//  TLTodoItemCellSubviews.m
//  TodoList
//
//  Created by AlexDenisov on 26/09/15.
//
//

#import "TLTodoItemCellSubviews.h"
#import "TLTodoItemCell_Internal.h"

@interface TLTodoItemCellSubviews ()

@property (nonatomic, weak) TLTodoItemCell *cell;

@end

@implementation TLTodoItemCellSubviews

- (instancetype)initWithCell:(TLTodoItemCell *)cell {
    self = [super init];

    self.cell = cell;
    [self initSubviews];

    return self;
}

- (void)initSubviews {
    self.cell.todoItemBodyLabel = [UILabel new];
    self.cell.todoItemBodyLabel.numberOfLines = 0;
    [self.cell addSubview:self.cell.todoItemBodyLabel];

    self.cell.editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cell addSubview:self.cell.editButton];

    self.cell.checkboxButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cell addSubview:self.cell.checkboxButton];

    self.cell.reorderButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cell addSubview:self.cell.reorderButton];
}

@end
