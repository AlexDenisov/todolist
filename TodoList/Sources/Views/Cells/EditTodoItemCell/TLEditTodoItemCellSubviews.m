//
//  TLEditTodoItemCellSubviews.m
//  TodoList
//
//  Created by AlexDenisov on 26/09/15.
//
//

#import "TLEditTodoItemCellSubviews.h"
#import "TLEditTodoItemCell_Internal.h"
#import "TLTextView.h"

@interface TLEditTodoItemCellSubviews ()

@property (nonatomic, weak) TLEditTodoItemCell *cell;

@end

@implementation TLEditTodoItemCellSubviews

- (instancetype)initWithCell:(TLEditTodoItemCell *)cell {
    self = [super init];

    self.cell = cell;
    [self initSubviews];

    return self;
}

- (void)initSubviews {
    self.cell.workingArea = [UIView new];
    [self.cell addSubview:self.cell.workingArea];

    self.cell.confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cell addSubview:self.cell.confirmButton];

    self.cell.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cell addSubview:self.cell.cancelButton];

    self.cell.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cell addSubview:self.cell.deleteButton];

    self.cell.inputTextView = [TLTextView new];
    [self.cell addSubview:self.cell.inputTextView];
}

@end
