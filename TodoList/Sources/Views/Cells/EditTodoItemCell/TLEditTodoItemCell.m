//
//  TLEditTodoItemCell.m
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import "TLEditTodoItemCell_Internal.h"
#import "TLEditTodoItemCellActions.h"
#import "TLEditTodoItemCellConstraints.h"
#import "TLEditTodoItemCellStyle.h"
#import "TLEditTodoItemCellSubviews.h"
#import "TLTodoItem.h"
#import "TLTextView.h"

@implementation TLEditTodoItemCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];

    self.cellSubviews = [[TLEditTodoItemCellSubviews alloc] initWithCell:self];
    self.actions = [[TLEditTodoItemCellActions alloc] initWithCell:self];
    self.cellConstraints = [[TLEditTodoItemCellConstraints alloc] initWithCell:self];
    self.style = [[TLEditTodoItemCellStyle alloc] initWithCell:self];

    return self;
}

- (void)updateUI {
    self.inputTextView.text = self.editableTodoItem.body;
    if (!self.editableTodoItem.isDeletable) {
        self.deleteButton.enabled = NO;
    } else {
        self.deleteButton.enabled = YES;
    }
}

- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection {
    if (self.traitCollection.verticalSizeClass == UIUserInterfaceSizeClassCompact) {
        [self compactLayout];
    } else {
        [self regularLayout];
    }
}

- (void)compactLayout {
    [self.cellConstraints compactLayout];
}

- (void)regularLayout {
    [self.cellConstraints regularLayout];
}

- (void)setEditableTodoItem:(TLTodoItem *)todoItem {
    _editableTodoItem = todoItem;
    [self updateUI];
}

- (void)setDelegate:(id<TLEditTodoItemCellDelegate>)delegate {
    [self.actions setDelegate:delegate];
}

- (id<TLEditTodoItemCellDelegate>)delegate {
    return [self.actions delegate];
}

@end
