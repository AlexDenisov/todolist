//
//  TLEditTodoItemCellStyle.m
//  TodoList
//
//  Created by AlexDenisov on 26/09/15.
//
//

#import "TLEditTodoItemCellStyle.h"
#import "TLEditTodoItemCell_Internal.h"
#import "TLTextView.h"
#import "TLPalette.h"

const static CGFloat kTLImageInset = 8.f;

@interface TLEditTodoItemCellStyle ()

@property (nonatomic, weak) TLEditTodoItemCell *cell;
@property (nonatomic, strong) TLPalette *palette;

@end

@implementation TLEditTodoItemCellStyle

- (instancetype)initWithCell:(TLEditTodoItemCell *)cell {
    self = [super init];

    self.cell = cell;
    self.palette = [TLPalette new];

    [self applyStyles];

    return self;
}

- (void)applyStyles {
    self.cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6];
    self.cell.workingArea.backgroundColor = self.palette.black;

    self.cell.inputTextView.backgroundColor = [UIColor clearColor];
    self.cell.inputTextView.textColor = self.palette.white;
    self.cell.inputTextView.font = [UIFont systemFontOfSize:22.f];
    self.cell.inputTextView.tintColor = self.palette.blue;

    [self setImageWithName:@"delete_icon" forButton:self.cell.deleteButton];
    [self setImageWithName:@"cancel_icon" forButton:self.cell.cancelButton];
    [self setImageWithName:@"confirm_icon" forButton:self.cell.confirmButton];

    self.cell.deleteButton.tintColor = self.palette.orange;
    self.cell.confirmButton.tintColor = self.palette.gray;
    self.cell.cancelButton.tintColor = self.palette.gray;
}

- (void)setImageWithName:(NSString *)imageName forButton:(UIButton *)button {
    button.contentMode = UIViewContentModeScaleAspectFit;
    UIEdgeInsets insets = UIEdgeInsetsMake(kTLImageInset, kTLImageInset, kTLImageInset, kTLImageInset);
    UIImage *image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [button setImage:image forState:UIControlStateNormal];
    [button setImageEdgeInsets:insets];
}

@end
