//
//  TLEditTodoItemCellConstraints.m
//  TodoList
//
//  Created by AlexDenisov on 25/09/15.
//
//

#import "TLEditTodoItemCellConstraints.h"
#import "TLEditTodoItemCell_Internal.h"
#import "TLTextView.h"

const static CGFloat kTLTextViewInset = 4;

const static CGFloat kTLCompactWidth = 300;
const static CGFloat kTLCompactHeight = 80;
const static CGFloat kTLCompactTopPadding = 10;

const static CGFloat kTLRegularWidth = 200;
const static CGFloat kTLRegularHeight = 200;
const static CGFloat kTLRegularTopPadding = 40;

const static CGFloat kTLButtonWidth = 36;
const static CGFloat kTLButtonHeight = 36;

@interface TLEditTodoItemCellConstraints ()

@property (nonatomic, weak) TLEditTodoItemCell *cell;

#pragma mark Working Area

@property (nonatomic, strong) NSLayoutConstraint *workingAreaTop;
@property (nonatomic, strong) NSLayoutConstraint *workingAreaWidth;
@property (nonatomic, strong) NSLayoutConstraint *workingAreaHeight;

#pragma mark Confirm Button

@property (nonatomic, strong) NSLayoutConstraint *confirmButtonWidth;

#pragma mark Cancel Button

@property (nonatomic, strong) NSLayoutConstraint *cancelButtonBottom;
@property (nonatomic, strong) NSLayoutConstraint *cancelButtonTrailing;
@property (nonatomic, strong) NSLayoutConstraint *cancelButtonHeight;

#pragma mark Delete Button

@property (nonatomic, strong) NSLayoutConstraint *deleteButtonBottom;
@property (nonatomic, strong) NSLayoutConstraint *deleteButtonWidth;

#pragma mark Cancel Button

@property (nonatomic, strong) NSLayoutConstraint *inputTextViewBottom;
@property (nonatomic, strong) NSLayoutConstraint *inputTextViewLeading;
@property (nonatomic, strong) NSLayoutConstraint *inputTextViewTrailing;

@end

@implementation TLEditTodoItemCellConstraints

- (instancetype)initWithCell:(TLEditTodoItemCell *)cell {
    self = [super init];

    self.cell = cell;

    [self initWorkingAreaConstraints];
    [self initConfirmButtonConstraints];
    [self initCancelButtonConstraints];
    [self initDeleteButtonConstraints];
    [self initInputTextViewConstraints];

    return self;
}

- (void)compactLayout {
    self.workingAreaWidth.constant = kTLCompactWidth;
    self.workingAreaHeight.constant = kTLCompactHeight;
    self.workingAreaTop.constant = kTLCompactTopPadding;

    self.cancelButtonBottom.constant = -self.workingAreaHeight.constant + self.cancelButtonHeight.constant;
    self.cancelButtonTrailing.constant = 0;

    self.deleteButtonBottom.constant = self.cancelButtonBottom.constant;

    self.inputTextViewLeading.constant = self.deleteButtonWidth.constant + kTLTextViewInset;
    self.inputTextViewTrailing.constant = -self.confirmButtonWidth.constant - kTLTextViewInset;
    self.inputTextViewBottom.constant = kTLTextViewInset;
}

- (void)regularLayout {
    self.workingAreaWidth.constant = kTLRegularWidth;
    self.workingAreaHeight.constant = kTLRegularHeight;
    self.workingAreaTop.constant = kTLRegularTopPadding;

    self.cancelButtonBottom.constant = 0;
    self.cancelButtonTrailing.constant = -self.confirmButtonWidth.constant;

    self.deleteButtonBottom.constant = 0;

    self.inputTextViewLeading.constant = kTLTextViewInset;
    self.inputTextViewTrailing.constant = -kTLTextViewInset;
    self.inputTextViewBottom.constant = -self.confirmButtonWidth.constant + kTLTextViewInset;
}

#pragma mark - Working Area

- (void)initWorkingAreaConstraints {
    self.cell.workingArea.translatesAutoresizingMaskIntoConstraints = NO;

    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.cell.workingArea
                                                           attribute:NSLayoutAttributeTop
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:self.cell
                                                           attribute:NSLayoutAttributeTop
                                                          multiplier:1
                                                            constant:0];

    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.cell.workingArea
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1
                                                              constant:0];

    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.cell.workingArea
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1
                                                               constant:0];

    NSLayoutConstraint *center = [NSLayoutConstraint constraintWithItem:self.cell.workingArea
                                                              attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cell
                                                              attribute:NSLayoutAttributeCenterX
                                                             multiplier:1
                                                               constant:0];

    [self.cell addConstraints:@[
                                height,
                                width,
                                top,
                                center
                                ]];

    self.workingAreaHeight = height;
    self.workingAreaWidth = width;
    self.workingAreaTop = top;
}

- (void)initConfirmButtonConstraints {
    self.cell.confirmButton.translatesAutoresizingMaskIntoConstraints = NO;

    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.cell.confirmButton
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1
                                                              constant:kTLButtonWidth];

    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.cell.confirmButton
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1
                                                               constant:kTLButtonHeight];

    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.cell.confirmButton
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cell.workingArea
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1
                                                               constant:0];

    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.cell.confirmButton
                                                                attribute:NSLayoutAttributeTrailing
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.cell.workingArea
                                                                attribute:NSLayoutAttributeTrailing
                                                               multiplier:1
                                                                 constant:0];

    [self.cell addConstraints:@[
                                width,
                                height,
                                trailing,
                                bottom
                                ]];

    self.confirmButtonWidth = width;
}

- (void)initCancelButtonConstraints {
    self.cell.cancelButton.translatesAutoresizingMaskIntoConstraints = NO;

    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.cell.cancelButton
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1
                                                              constant:kTLButtonWidth];

    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.cell.cancelButton
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1
                                                               constant:kTLButtonHeight];

    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.cell.cancelButton
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cell.workingArea
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1
                                                               constant:0];

    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.cell.cancelButton
                                                                attribute:NSLayoutAttributeTrailing
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.cell.workingArea
                                                                attribute:NSLayoutAttributeTrailing
                                                               multiplier:1
                                                                 constant:0];

    [self.cell addConstraints:@[
                                width,
                                height,
                                trailing,
                                bottom
                                ]];

    self.cancelButtonBottom = bottom;
    self.cancelButtonTrailing = trailing;
    self.cancelButtonHeight = height;
}

- (void)initDeleteButtonConstraints {
    self.cell.deleteButton.translatesAutoresizingMaskIntoConstraints = NO;

    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:self.cell.deleteButton
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1
                                                              constant:kTLButtonWidth];

    NSLayoutConstraint *height = [NSLayoutConstraint constraintWithItem:self.cell.deleteButton
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1
                                                               constant:kTLButtonHeight];

    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.cell.deleteButton
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cell.workingArea
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1
                                                               constant:0];

    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.cell.deleteButton
                                                               attribute:NSLayoutAttributeLeading
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.cell.workingArea
                                                               attribute:NSLayoutAttributeLeading
                                                              multiplier:1
                                                                constant:0];

    [self.cell addConstraints:@[
                                width,
                                height,
                                leading,
                                bottom
                                ]];

    self.deleteButtonBottom = bottom;
    self.deleteButtonWidth = width;
}

- (void)initInputTextViewConstraints {
    self.cell.inputTextView.translatesAutoresizingMaskIntoConstraints = NO;
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.cell.workingArea
                                                           attribute:NSLayoutAttributeTop
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:self.cell.inputTextView
                                                           attribute:NSLayoutAttributeTop
                                                          multiplier:1
                                                            constant:kTLTextViewInset];

    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:self.cell.inputTextView
                                                              attribute:NSLayoutAttributeBottom
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.cell.workingArea
                                                              attribute:NSLayoutAttributeBottom
                                                             multiplier:1
                                                               constant:0];

    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.cell.inputTextView
                                                               attribute:NSLayoutAttributeLeading
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self.cell.workingArea
                                                               attribute:NSLayoutAttributeLeading
                                                              multiplier:1
                                                                constant:0];

    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.cell.inputTextView
                                                                attribute:NSLayoutAttributeTrailing
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.cell.workingArea
                                                                attribute:NSLayoutAttributeTrailing
                                                               multiplier:1
                                                                 constant:0];

    [self.cell addConstraints:@[
                                top,
                                bottom,
                                leading,
                                trailing
                                ]];

    self.inputTextViewBottom = bottom;
    self.inputTextViewLeading = leading;
    self.inputTextViewTrailing = trailing;
}

@end
