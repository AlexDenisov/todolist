//
//  TLEditTodoItemCell.h
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import <UIKit/UIKit.h>

@class TLTodoItem;
@protocol TLEditTodoItemCellDelegate;

@interface TLEditTodoItemCell : UICollectionViewCell

@property (nonatomic, weak) id<TLEditTodoItemCellDelegate> delegate;
@property (nonatomic, strong) TLTodoItem *editableTodoItem;

@end
