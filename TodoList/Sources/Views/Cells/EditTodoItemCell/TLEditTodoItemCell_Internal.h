//
//  TLEditTodoItemCell_Internal.h
//  TodoList
//
//  Created by AlexDenisov on 26/09/15.
//
//

#import "TLEditTodoItemCell.h"

@class TLEditTodoItemCellActions;
@class TLEditTodoItemCellConstraints;
@class TLEditTodoItemCellStyle;
@class TLEditTodoItemCellSubviews;
@class TLTextView;

@interface TLEditTodoItemCell ()

@property (nonatomic, strong) UIView *workingArea;
@property (nonatomic, strong) UIButton *confirmButton;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) TLTextView *inputTextView;

@property (nonatomic, strong) TLEditTodoItemCellActions *actions;
@property (nonatomic, strong) TLEditTodoItemCellConstraints *cellConstraints;
@property (nonatomic, strong) TLEditTodoItemCellStyle *style;
@property (nonatomic, strong) TLEditTodoItemCellSubviews *cellSubviews;

@end
