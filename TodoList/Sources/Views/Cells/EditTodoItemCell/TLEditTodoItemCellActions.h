//
//  TLEditTodoItemCellActions.h
//  TodoList
//
//  Created by AlexDenisov on 25/09/15.
//
//

#import <Foundation/Foundation.h>

@class TLEditTodoItemCell;
@protocol TLEditTodoItemCellDelegate;

@interface TLEditTodoItemCellActions : NSObject

@property (nonatomic, weak) id<TLEditTodoItemCellDelegate> delegate;

- (instancetype)initWithCell:(TLEditTodoItemCell *)cell;

@end
