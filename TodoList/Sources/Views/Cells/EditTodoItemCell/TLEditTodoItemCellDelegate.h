//
//  TLEditTodoItemCellDelegate.h
//  TodoList
//
//  Created by AlexDenisov on 25/09/15.
//
//

#import <Foundation/Foundation.h>

@class TLEditTodoItemCell;
@class TLTodoItem;

@protocol TLEditTodoItemCellDelegate
    <NSObject>

- (void)editTodoItemCell:(TLEditTodoItemCell *)cell didConfirmEditingOfTodoItem:(TLTodoItem *)todoItem;
- (void)editTodoItemCell:(TLEditTodoItemCell *)cell didConfirmDeletingOfTodoItem:(TLTodoItem *)todoItem;
- (void)editTodoItemCellDidCancelEditing:(TLEditTodoItemCell *)cell;

@end
