//
//  TLEditTodoItemCellSubviews.h
//  TodoList
//
//  Created by AlexDenisov on 26/09/15.
//
//

#import <Foundation/Foundation.h>

@class TLEditTodoItemCell;

@interface TLEditTodoItemCellSubviews : NSObject

- (instancetype)initWithCell:(TLEditTodoItemCell *)cell;

@end
