//
//  TLEditTodoItemCellActions.m
//  TodoList
//
//  Created by AlexDenisov on 25/09/15.
//
//

#import "TLEditTodoItemCellActions.h"
#import "TLEditTodoItemCell_Internal.h"
#import "TLEditTodoItemCellDelegate.h"
#import "TLTodoItem.h"
#import "TLTextView.h"

@interface TLEditTodoItemCellActions ()

@property (nonatomic, weak) TLEditTodoItemCell *cell;

@end

@implementation TLEditTodoItemCellActions

- (instancetype)initWithCell:(TLEditTodoItemCell *)cell {
    self = [super init];

    self.cell = cell;
    [self addTargetsAndActions];

    return self;
}

- (void)addTargetsAndActions {
    [self.cell.cancelButton addTarget:self
                               action:@selector(cancelButtonTapped:)
                     forControlEvents:UIControlEventTouchUpInside];

    [self.cell.deleteButton addTarget:self
                               action:@selector(deleteButtonTapped:)
                     forControlEvents:UIControlEventTouchUpInside];

    [self.cell.confirmButton addTarget:self
                                action:@selector(confirmButtonTapped:)
                      forControlEvents:UIControlEventTouchUpInside];
}

- (void)confirmButtonTapped:(UIButton *)confirmButton {
    self.cell.editableTodoItem.body = self.cell.inputTextView.text;
    [self.delegate editTodoItemCell:self.cell
        didConfirmEditingOfTodoItem:self.cell.editableTodoItem];
}

- (void)cancelButtonTapped:(UIButton *)cancelButton {
    [self.delegate editTodoItemCellDidCancelEditing:self.cell];
}

- (void)deleteButtonTapped:(UIButton *)cancelButton {
    [self.delegate editTodoItemCell:self.cell
       didConfirmDeletingOfTodoItem:self.cell.editableTodoItem];
}

@end
