//
//  TLEditTodoItemCellStyle.h
//  TodoList
//
//  Created by AlexDenisov on 26/09/15.
//
//

#import <Foundation/Foundation.h>

@class TLEditTodoItemCell;

@interface TLEditTodoItemCellStyle : NSObject

- (instancetype)initWithCell:(TLEditTodoItemCell *)cell;

@end
