//
//  TLEditTodoItemCellConstraints.h
//  TodoList
//
//  Created by AlexDenisov on 25/09/15.
//
//

#import <Foundation/Foundation.h>

@class TLEditTodoItemCell;

@interface TLEditTodoItemCellConstraints : NSObject

- (instancetype)initWithCell:(TLEditTodoItemCell *)cell;
- (void)regularLayout;
- (void)compactLayout;

@end
