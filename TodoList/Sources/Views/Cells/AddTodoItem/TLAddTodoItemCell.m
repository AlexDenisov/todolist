//
//  TLAddTodoItemCell.m
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import "TLAddTodoItemCell.h"
#import "TLPalette.h"

@interface TLAddTodoItemCell ()

@property (nonatomic, strong) UILabel *label;

@end

@implementation TLAddTodoItemCell

// needs to be improved...

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];

    TLPalette *palette = [TLPalette new];

    self.label = [UILabel new];
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.text = @"+";
    self.label.font = [UIFont systemFontOfSize:44];
    self.label.textColor = palette.white;

    [self addSubview:self.label];

    self.backgroundColor = palette.blue;

    CALayer *layer = self.layer;
    layer.masksToBounds = NO;
    layer.shadowOpacity = 0.75;
    layer.shadowRadius = 3;
    layer.shadowOffset = CGSizeZero;
    layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    layer.shouldRasterize = YES;
    layer.shadowColor = palette.blue.CGColor;

    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.label.frame = self.bounds;
}

@end
