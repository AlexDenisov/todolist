//
//  TLTextView.m
//  TodoList
//
//  Created by AlexDenisov on 27/09/15.
//
//

#import "TLTextView.h"
#import "TLPalette.h"

const static CGFloat kTLPlaceholderOffset = 4;

@interface TLTextView ()

@property (nonatomic, strong) UILabel *placeholderLabel;

@end

@implementation TLTextView

- (instancetype)init {
    self = [super init];

    [self initPlaceholder];
    self.placeholderLabel.text = NSLocalizedString(@"edit_todo_item.placeholder_text", nil);

    return self;
}

- (void)initPlaceholder {
    TLPalette *palette = [TLPalette new];

    self.placeholderLabel = [UILabel new];
    [self addSubview:self.placeholderLabel];

    self.placeholderLabel.font = [UIFont systemFontOfSize:22];
    self.placeholderLabel.textColor = palette.gray;
    self.placeholderLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.placeholderLabel.numberOfLines = 0;

    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.placeholderLabel
                                                           attribute:NSLayoutAttributeTop
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:self
                                                           attribute:NSLayoutAttributeTop
                                                          multiplier:1
                                                            constant:kTLPlaceholderOffset];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.placeholderLabel
                                                               attribute:NSLayoutAttributeLeading
                                                               relatedBy:NSLayoutRelationEqual
                                                                  toItem:self
                                                               attribute:NSLayoutAttributeLeading
                                                              multiplier:1
                                                                constant:kTLPlaceholderOffset];
    NSLayoutConstraint *trailing = [NSLayoutConstraint constraintWithItem:self.placeholderLabel
                                                                attribute:NSLayoutAttributeTrailing
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self
                                                                attribute:NSLayoutAttributeTrailing
                                                               multiplier:1
                                                                 constant:kTLPlaceholderOffset];
    [self addConstraints:@[
                           top,
                           leading,
                           trailing
                           ]];
}

- (BOOL)becomeFirstResponder {
    [self hidePlaceholder];
    return [super becomeFirstResponder];
}

- (BOOL)resignFirstResponder {
    if (!self.text.length) {
        [self showPlaceholder];
    }
    return [super resignFirstResponder];
}

- (void)setText:(NSString *)text {
    [super setText:text];
    if (self.text.length) {
        [self hidePlaceholder];
    } else {
        [self showPlaceholder];
    }
}

- (void)showPlaceholder {
    self.placeholderLabel.alpha = 1;
}

- (void)hidePlaceholder {
    if (self.placeholderLabel.alpha == 0) {
        return;
    }
    self.placeholderLabel.alpha = 0;
}

@end
