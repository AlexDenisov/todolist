//
//  TLTodoItem.m
//  TodoList
//
//  Created by AlexDenisov on 25/09/15.
//
//

#import "TLTodoItem.h"

@implementation TLTodoItem

- (BOOL)isNew {
    return self.isPersistent == NO;
}

- (BOOL)isDeletable {
    return self.isPersistent == YES;
}

@end
