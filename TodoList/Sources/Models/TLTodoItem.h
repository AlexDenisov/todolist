//
//  TLTodoItem.h
//  TodoList
//
//  Created by AlexDenisov on 25/09/15.
//
//

#import <Foundation/Foundation.h>

@interface TLTodoItem : NSObject

@property (nonatomic, copy) NSString *body;
@property (nonatomic, getter = isPersistent) BOOL persistent;
@property (nonatomic, getter = isDone) BOOL done;

- (BOOL)isNew;
- (BOOL)isDeletable;

@end
