//
//  TLViewControllerFactory.m
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import "TLViewControllerFactory.h"
#import "TLHomeScreenViewController.h"
#import "TLDataController.h"

@implementation TLViewControllerFactory

- (TLHomeScreenViewController *)homeScreen {
    TLHomeScreenViewController *homeScreen = [TLHomeScreenViewController new];
    homeScreen.dataController = [TLDataController new];
    return homeScreen;
}

- (UIViewController *)rootViewController {
    TLHomeScreenViewController *homeScreen = self.homeScreen;
    UINavigationController *rootController = [[UINavigationController alloc] initWithRootViewController:homeScreen];
    return rootController;
}

@end
