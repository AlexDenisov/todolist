//
//  TLViewControllerFactory.h
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import <UIKit/UIKit.h>

@class TLHomeScreenViewController;

@interface TLViewControllerFactory : NSObject

- (TLHomeScreenViewController *)homeScreen;
- (UIViewController *)rootViewController;

@end
