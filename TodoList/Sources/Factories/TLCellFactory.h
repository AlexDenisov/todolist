//
//  TLCellFactory.h
//  TodoList
//
//  Created by AlexDenisov on 25/09/15.
//
//

#import <UIKit/UIKit.h>

@class TLTodoItemCell;
@class TLEditTodoItemCell;
@class TLAddTodoItemCell;

@interface TLCellFactory : NSObject

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView;

- (TLTodoItemCell *)todoItemCellAtIndexPath:(NSIndexPath *)indexPath;
- (TLAddTodoItemCell *)addTodoItemCellAtIndexPath:(NSIndexPath *)indexPath;
- (TLEditTodoItemCell *)editTodoItemCellAtIndexPath:(NSIndexPath *)indexPath;

@end
