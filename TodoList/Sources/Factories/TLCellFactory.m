//
//  TLCellFactory.m
//  TodoList
//
//  Created by AlexDenisov on 25/09/15.
//
//

#import "TLCellFactory.h"

#import "TLTodoItemCell.h"
#import "TLAddTodoItemCell.h"
#import "TLEditTodoItemCell.h"

@interface TLCellFactory ()

@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation TLCellFactory

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView {
    self = [super init];

    self.collectionView = collectionView;
    [self registerCells];

    return self;
}

- (TLTodoItemCell *)todoItemCellAtIndexPath:(NSIndexPath *)indexPath {
    return [self.collectionView dequeueReusableCellWithReuseIdentifier:self.todoItemCellIdentifier
                                                          forIndexPath:indexPath];
}

- (TLEditTodoItemCell *)editTodoItemCellAtIndexPath:(NSIndexPath *)indexPath {
    return [self.collectionView dequeueReusableCellWithReuseIdentifier:self.editTodoItemCellIdentifier
                                                          forIndexPath:indexPath];
}

- (TLAddTodoItemCell *)addTodoItemCellAtIndexPath:(NSIndexPath *)indexPath {
    return [self.collectionView dequeueReusableCellWithReuseIdentifier:self.addTodoItemCellIdentifier
                                                          forIndexPath:indexPath];
}

#pragma mark - Cell Registration

- (void)registerCells {
    [self.collectionView registerClass:[TLTodoItemCell class]
            forCellWithReuseIdentifier:self.todoItemCellIdentifier];
    [self.collectionView registerClass:[TLAddTodoItemCell class]
            forCellWithReuseIdentifier:self.addTodoItemCellIdentifier];
    [self.collectionView registerClass:[TLEditTodoItemCell class]
            forCellWithReuseIdentifier:self.editTodoItemCellIdentifier];
}

#pragma mark - Cell Identifiers

- (NSString *)todoItemCellIdentifier {
    return NSStringFromClass([TLTodoItemCell class]);
}

- (NSString *)addTodoItemCellIdentifier {
    return NSStringFromClass([TLAddTodoItemCell class]);
}

- (NSString *)editTodoItemCellIdentifier {
    return NSStringFromClass([TLEditTodoItemCell class]);
}

@end
