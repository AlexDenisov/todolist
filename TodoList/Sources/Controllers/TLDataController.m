//
//  TLDataController.m
//  TodoList
//
//  Created by AlexDenisov on 27/09/15.
//
//

#import "TLDataController.h"
#import "TLTodoItem.h"

@interface TLDataController ()

@property (nonatomic, strong) NSMutableArray *internalStorage;

@end

@implementation TLDataController

- (instancetype)init {
    self = [super init];

    self.internalStorage = [NSMutableArray new];

    return self;
}

- (NSUInteger)numberOfTodoItems {
    return self.internalStorage.count;
}

- (TLTodoItem *)todoItemAtIndex:(NSUInteger)index {
    return self.internalStorage[index];
}

- (void)addTodoItem:(TLTodoItem *)todoItem {
    todoItem.persistent = YES;
    [self.internalStorage addObject:todoItem];
}

- (void)updateTodoItem:(TLTodoItem *)todoItem {
    // weird method
    NSUInteger index = [self.internalStorage indexOfObject:todoItem];
    self.internalStorage[index] = todoItem;
}

- (void)deleteTodoItem:(TLTodoItem *)todoItem {
    [self.internalStorage removeObject:todoItem];
}

- (void)moveTodoItemAtIndex:(NSUInteger)source toIndex:(NSUInteger)destination {
    TLTodoItem *item = [self todoItemAtIndex:source];
    [self deleteTodoItem:item];
    [self.internalStorage insertObject:item atIndex:destination];
}

- (NSArray *)todoItems {
    return self.internalStorage;
}

@end
