//
//  TLAppController.m
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import "TLAppController.h"
#import "TLViewControllerFactory.h"

@interface TLAppController ()

@property (nonatomic, strong) UIWindow *mainWindow;

@end

@implementation TLAppController

- (instancetype)initWithMainWindow:(UIWindow *)mainWindow {
    self = [super init];

    self.mainWindow = mainWindow;

    return self;
}

- (void)showHomeScreen {
    TLViewControllerFactory *factory = [TLViewControllerFactory new];
    self.mainWindow.rootViewController = factory.rootViewController;
    [self.mainWindow makeKeyAndVisible];
}

@end
