//
//  TLDataController.h
//  TodoList
//
//  Created by AlexDenisov on 27/09/15.
//
//

#import <Foundation/Foundation.h>

@class TLTodoItem;

@interface TLDataController : NSObject

- (NSUInteger)numberOfTodoItems;
- (TLTodoItem *)todoItemAtIndex:(NSUInteger)index;
- (NSArray *)todoItems;

- (void)addTodoItem:(TLTodoItem *)todoItem;
- (void)updateTodoItem:(TLTodoItem *)todoItem;
- (void)deleteTodoItem:(TLTodoItem *)todoItem;

- (void)moveTodoItemAtIndex:(NSUInteger)source toIndex:(NSUInteger)destination;

@end
