//
//  TLAppController.h
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import <UIKit/UIKit.h>

@interface TLAppController : NSObject

+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;


- (instancetype)initWithMainWindow:(UIWindow *)mainWindow;
- (void)showHomeScreen;

@end
