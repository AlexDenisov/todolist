//
//  TLCollectionViewFlowLayout.h
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import <UIKit/UIKit.h>

@interface TLCollectionViewFlowLayout : UICollectionViewFlowLayout

@property (nonatomic) NSUInteger editTodoItemSection;

@end
