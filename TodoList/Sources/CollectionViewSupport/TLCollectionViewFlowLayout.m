//
//  TLCollectionViewFlowLayout.m
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import "TLCollectionViewFlowLayout.h"

@implementation TLCollectionViewFlowLayout

- (UICollectionViewLayoutAttributes *)modifiedAttributesFromAttributes:(UICollectionViewLayoutAttributes *)attributes {
    if (attributes.indexPath.section != self.editTodoItemSection) {
        return attributes;
    }
    // in editing mode show full screen EditTodoItemCell
    UICollectionViewLayoutAttributes *modifiedAttributes = [attributes copy];
    CGFloat contentOffset = self.collectionView.contentOffset.y;
    CGFloat contentInset = self.collectionView.contentInset.top;
    CGRect editCellFrame = self.collectionView.bounds;
    editCellFrame.origin.y = contentInset + contentOffset;
    editCellFrame.size.height -= contentInset;

    modifiedAttributes.frame = editCellFrame;
    // show cell on top, just in case
    modifiedAttributes.zIndex = 5;

    return modifiedAttributes;
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    // weird, but after rotation layout stays the same, so it's important to invalidate it manually from ViewController
    return YES;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray *originalAttributes = [super layoutAttributesForElementsInRect:rect];
    NSMutableArray *modifiedAttributes = [NSMutableArray arrayWithCapacity:originalAttributes.count];
    for (UICollectionViewLayoutAttributes *attributes in originalAttributes) {
        [modifiedAttributes addObject:[self modifiedAttributesFromAttributes:attributes]];
    }

    return modifiedAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewLayoutAttributes *attributes = [super layoutAttributesForItemAtIndexPath:indexPath];
    return [self modifiedAttributesFromAttributes:attributes];
}

@end
