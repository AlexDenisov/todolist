//
//  TLCollectionViewDragging.h
//  TodoList
//
//  Created by AlexDenisov on 27/09/15.
//
//

#import <UIKit/UIKit.h>

@interface TLCollectionViewDragging : NSObject

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView;

@end
