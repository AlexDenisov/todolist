//
//  TLCollectionViewDragging.m
//  TodoList
//
//  Created by AlexDenisov on 27/09/15.
//
//

#import "TLCollectionViewDragging.h"
#import "TLTodoItemCell.h"


@interface TLCollectionViewDragging ()
    <UIGestureRecognizerDelegate>

@property (nonatomic, weak) UICollectionView *collectionView;

@end

@implementation TLCollectionViewDragging

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView {
    self = [super init];

    self.collectionView = collectionView;
    UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(collectionViewPanned:)];
    gesture.delegate = self;
    [self.collectionView addGestureRecognizer:gesture];

    return self;
}

- (void)collectionViewPanned:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint location = [gestureRecognizer locationInView:self.collectionView];
    switch (gestureRecognizer.state) {
        case UIGestureRecognizerStateBegan: {
            NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:location];
            [self.collectionView beginInteractiveMovementForItemAtIndexPath:indexPath];
        } break;
        case UIGestureRecognizerStateChanged: {
            // dirty hack to prevent moving of AddCell
            NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:location];
            TLTodoItemCell *cell = (TLTodoItemCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            if (cell && ![cell isKindOfClass:[TLTodoItemCell class]]) {
                return;
            }
            [self.collectionView updateInteractiveMovementTargetPosition:location];
        } break;
        case UIGestureRecognizerStateEnded: {
            [self.collectionView endInteractiveMovement];
        } break;
        default: {
            [self.collectionView cancelInteractiveMovement];
        } break;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint location = [gestureRecognizer locationInView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:location];
    if (!indexPath) {
        return NO;
    }

    TLTodoItemCell *cell = (TLTodoItemCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    if (![cell isKindOfClass:[TLTodoItemCell class]]) {
        return NO;
    }
    CGPoint locationInCell = [gestureRecognizer locationInView:cell];
    return [cell isDraggableAtLocation:locationInCell];
}

@end
