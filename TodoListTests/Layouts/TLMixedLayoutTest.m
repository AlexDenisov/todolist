//
//  TLMixedLayoutTest.m
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import <XCTest/XCTest.h>
#import "TLMixedLayout.h"
#import "TLTodoItemCell.h"
#import "TLTodoItem.h"
#import "TLAddTodoItemCell.h"
#import "TLCollectionViewFlowLayout.h"
#import "TLDataController.h"

static TLDataController *dataControler(NSArray *dataSource) {
    TLDataController *controller = [TLDataController new];
    for (TLTodoItem *item in dataSource) {
        [controller addTodoItem:item];
    }
    return controller;
}

static UICollectionView *collectionView() {
    UICollectionViewFlowLayout *layout = [TLCollectionViewFlowLayout new];
    return [[UICollectionView alloc] initWithFrame:CGRectZero
                              collectionViewLayout:layout];
}

static TLMixedLayout *layoutWithDataSource(NSArray *dataSource) {
    TLMixedLayout *layout = [[TLMixedLayout alloc] initWithCollectionView:collectionView()
                                                           dataController:dataControler(dataSource)];
    [layout prepareLayout];
    return layout;
}

@interface TLMixedLayoutTest : XCTestCase

@end

@implementation TLMixedLayoutTest

- (void)test_TodoItemCell {
    id<UICollectionViewDataSource> layout = (id<UICollectionViewDataSource>)layoutWithDataSource(@[[TLTodoItem new], [TLTodoItem new]]);
    NSIndexPath *firstCellPath = [NSIndexPath indexPathForItem:0 inSection:0];
    UICollectionViewCell *cell = [layout collectionView:collectionView()cellForItemAtIndexPath:firstCellPath];
    XCTAssertTrue([cell isKindOfClass:[TLTodoItemCell class]]);
}

- (void)test_AddTodoItemCell {
    id<UICollectionViewDataSource> layout = (id<UICollectionViewDataSource>)layoutWithDataSource(@[]);
    NSIndexPath *firstCellPath = [NSIndexPath indexPathForItem:0 inSection:0];
    UICollectionViewCell *cell = [layout collectionView:collectionView()cellForItemAtIndexPath:firstCellPath];
    XCTAssertTrue([cell isKindOfClass:[TLAddTodoItemCell class]]);
}

@end
