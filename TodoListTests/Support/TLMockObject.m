//
//  TLMockObject.m
//  TodoList
//
//  Created by AlexDenisov on 25/09/15.
//
//

#import "TLMockObject.h"
#import <objc/runtime.h>

static id instanceMethodResolver(id self, SEL _cmd, ...);

@interface TLMockObject ()

@property (nonatomic, strong) NSMutableDictionary *receivedMessages;

@end

@implementation TLMockObject

- (instancetype)init {
    self = [super init];

    self.receivedMessages = [NSMutableDictionary new];

    return self;
}

- (NSUInteger)amountOfReceivedMessages:(SEL)selector {
    NSString *messageName = NSStringFromSelector(selector);
    NSNumber *amount = self.receivedMessages[messageName];
    return [amount unsignedIntegerValue];
}

- (void)registerReceivedMessage:(SEL)selector {
    NSString *messageName = NSStringFromSelector(selector);
    NSUInteger amount = [self amountOfReceivedMessages:selector];
    self.receivedMessages[messageName] = @(++amount);
}

+ (BOOL)resolveInstanceMethod:(SEL)sel {
    class_addMethod([self class], sel, (IMP) instanceMethodResolver, NULL);
    return YES;
}

- (BOOL)hasReceivedMessage:(SEL)selector {
    return [self amountOfReceivedMessages:selector] != 0;
}

@end

static id instanceMethodResolver(id self, SEL _cmd, ...) {
    TLMockObject *mock = self;
    [mock registerReceivedMessage:_cmd];
    return nil;
}
