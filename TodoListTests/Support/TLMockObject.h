//
//  TLMockObject.h
//  TodoList
//
//  Created by AlexDenisov on 25/09/15.
//
//

#import <Foundation/Foundation.h>

@interface TLMockObject : NSObject

- (BOOL)hasReceivedMessage:(SEL)selector;

@end
