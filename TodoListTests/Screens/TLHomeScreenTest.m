//
//  TLHomeScreenTest.m
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import <XCTest/XCTest.h>
#import "TLViewControllerFactory.h"
#import "TLHomeScreenViewController.h"

@interface TLHomeScreenTest : XCTestCase

@property (nonatomic, strong) TLHomeScreenViewController *subject;

@end

@implementation TLHomeScreenTest

- (void)setUp {
    [super setUp];
    TLViewControllerFactory *factory = [TLViewControllerFactory new];
    UIViewController *root = factory.rootViewController;
    self.subject = root.childViewControllers.firstObject;
}

- (void)test_HomeScreen_hasCollectionView {
    UIView *homeScreenView = self.subject.view;
    UIView *collectionView = homeScreenView.subviews.firstObject;
    XCTAssertTrue([collectionView isKindOfClass:[UICollectionView class]]);
}

@end
