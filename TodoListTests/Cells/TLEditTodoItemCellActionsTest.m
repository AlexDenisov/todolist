//
//  TLEditTodoItemCellActionsTest.m
//  TodoList
//
//  Created by AlexDenisov on 25/09/15.
//
//

#import <XCTest/XCTest.h>
#import "TLEditTodoItemCell_Internal.h"
#import "TLEditTodoItemCellDelegate.h"
#import "TLMockObject.h"

@interface TLEditTodoItemCellActionsTest : XCTestCase

@property (nonatomic, strong) TLEditTodoItemCell *subject;
@property (nonatomic, strong) TLMockObject *mockDelegate;

@end

@implementation TLEditTodoItemCellActionsTest

- (void)setUp {
    [super setUp];

    self.subject = [[TLEditTodoItemCell alloc] initWithFrame:CGRectZero];

    // use id to 'satisfy' type-checker
    id mock = [TLMockObject new];
    self.subject.delegate = mock;
    self.mockDelegate = mock;
}

- (void)test_ConfirmButtonTapHandling {
    [self.subject.confirmButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    XCTAssertTrue([self.mockDelegate hasReceivedMessage:@selector(editTodoItemCell:didConfirmEditingOfTodoItem:)]);
}

- (void)test_CancelButtonTapHandling {
    [self.subject.cancelButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    XCTAssertTrue([self.mockDelegate hasReceivedMessage:@selector(editTodoItemCellDidCancelEditing:)]);
}

- (void)test_DeleteButtonTapHandling {
    [self.subject.deleteButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    XCTAssertTrue([self.mockDelegate hasReceivedMessage:@selector(editTodoItemCell:didConfirmDeletingOfTodoItem:)]);
}

@end
