//
//  TLTodoItemCellActionsTest.m
//  TodoList
//
//  Created by AlexDenisov on 27/09/15.
//
//

#import <XCTest/XCTest.h>
#import "TLTodoItemCell_Internal.h"
#import "TLTodoItemCellDelegate.h"
#import "TLMockObject.h"

@interface TLTodoItemCellActionsTest : XCTestCase

@property (nonatomic, strong) TLTodoItemCell *subject;
@property (nonatomic, strong) TLMockObject *mockDelegate;

@end

@implementation TLTodoItemCellActionsTest

- (void)setUp {
    [super setUp];
    self.subject = [[TLTodoItemCell alloc] initWithFrame:CGRectZero];

    // use id to 'satisfy' type-checker
    id mock = [TLMockObject new];
    self.subject.delegate = mock;
    self.mockDelegate = mock;
}

- (void)test_EditButtonTapHandling {
    [self.subject.editButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    XCTAssertTrue([self.mockDelegate hasReceivedMessage:@selector(todoItemCell:didConfirmEditingOfTodoItem:)]);
}

- (void)test_DeleteButtonTapHandling {
    [self.subject.checkboxButton sendActionsForControlEvents:UIControlEventTouchUpInside];
    XCTAssertTrue([self.mockDelegate hasReceivedMessage:@selector(todoItemCell:didChangeDoneStateOfTodoItem:)]);
}

@end
