//
//  TLTodoItemTest.m
//  TodoList
//
//  Created by AlexDenisov on 25/09/15.
//
//

#import <XCTest/XCTest.h>
#import "TLTodoItem.h"

@interface TLTodoItemTest : XCTestCase

@end

@implementation TLTodoItemTest

- (void)test_IsNotPersistentByDefault {
    TLTodoItem *item = [TLTodoItem new];
    XCTAssertFalse(item.isPersistent, @"Newly created TodoItem is not persistent");
}

- (void)test_IsNewByDefault {
    TLTodoItem *item = [TLTodoItem new];
    XCTAssertTrue(item.isNew, @"Item considered new if it's not persistent");
}

- (void)test_IsNotDeletableByDefault {
    TLTodoItem *item = [TLTodoItem new];
    XCTAssertFalse(item.isDeletable, @"Item cannot be deleted if it's not persistent");
}

- (void)test_IsNotNew {
    TLTodoItem *item = [TLTodoItem new];
    item.persistent = YES;
    XCTAssertFalse(item.isNew, @"Item is not new if it's persistent");
}

- (void)test_IsDeletable {
    TLTodoItem *item = [TLTodoItem new];
    item.persistent = YES;
    XCTAssertTrue(item.isDeletable, @"Item considered deletable if it's persistent");
}

- (void)test_IsNotDoneByDefault {
    TLTodoItem *item = [TLTodoItem new];
    XCTAssertFalse(item.isDone, @"Newle created item is not done");
}

@end
