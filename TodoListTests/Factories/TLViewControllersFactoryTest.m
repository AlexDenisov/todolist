//
//  TLViewControllersFactory.m
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import <XCTest/XCTest.h>
#import "TLViewControllerFactory.h"
#import "TLHomeScreenViewController.h"

@interface TLViewControllersFactoryTest : XCTestCase

@property (nonatomic, strong) TLViewControllerFactory *subject;

@end

@implementation TLViewControllersFactoryTest

- (void)setUp {
    [super setUp];
    self.subject = [TLViewControllerFactory new];
}

- (void)test_HomeScreen {
    XCTAssertNotNil(self.subject.homeScreen, @"Instantiates HomeScreen ViewController");
}

- (void)test_RootViewController {
    XCTAssertNotNil(self.subject.rootViewController, @"Instantiates RootViewControler");
}

- (void)test_RootViewController_with_HomeScreen {
    UIViewController *root = self.subject.rootViewController;
    UIViewController *homeScreen = root.childViewControllers.firstObject;
    XCTAssertTrue([homeScreen isKindOfClass:[TLHomeScreenViewController class]], @"Instantiates RootViewControler with HomeScreen as a child");
}

@end
