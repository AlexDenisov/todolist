//
//  TLCellFactoryTest.m
//  TodoList
//
//  Created by AlexDenisov on 25/09/15.
//
//

#import <XCTest/XCTest.h>
#import "TLCellFactory.h"

#import "TLTodoItemCell.h"
#import "TLEditTodoItemCell.h"
#import "TLAddTodoItemCell.h"

static UICollectionView *collectionView() {
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    return [[UICollectionView alloc] initWithFrame:CGRectZero
                              collectionViewLayout:layout];
}


// If you call `dequeReuseableCell...` on collection view,
// that doesn't have data source or data source has zero
// elements - you will get EXC_BAD_ACCESS
// hence we need this trick
@interface StubDataSource : NSObject
    <UICollectionViewDataSource>
@end

@implementation StubDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [[UICollectionViewCell alloc] initWithFrame:CGRectZero];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}

@end

@interface TLCellFactoryTest : XCTestCase

@property (nonatomic, strong) TLCellFactory *subject;

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) StubDataSource *collectionViewDataSource;
@property (nonatomic, strong) NSIndexPath *indexPath;

@end

@implementation TLCellFactoryTest

- (void)setUp {
    [super setUp];
    self.indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    self.collectionView = collectionView();
    self.collectionViewDataSource = [StubDataSource new];
    self.collectionView.dataSource = self.collectionViewDataSource;

    self.subject = [[TLCellFactory alloc] initWithCollectionView:self.collectionView];
}

- (void)test_TodoItemCellCreation {
    UICollectionViewCell *cell = [self.subject todoItemCellAtIndexPath:self.indexPath];
    XCTAssertTrue([cell isKindOfClass:[TLTodoItemCell class]]);
}

- (void)test_EditTodoItemCellCreation {
    UICollectionViewCell *cell = [self.subject editTodoItemCellAtIndexPath:self.indexPath];
    XCTAssertTrue([cell isKindOfClass:[TLEditTodoItemCell class]]);
}

- (void)test_AddTodoItemCellCreation {
    UICollectionViewCell *cell = [self.subject addTodoItemCellAtIndexPath:self.indexPath];
    XCTAssertTrue([cell isKindOfClass:[TLAddTodoItemCell class]]);
}

@end
