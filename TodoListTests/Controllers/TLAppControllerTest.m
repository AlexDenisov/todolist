//
//  TLAppControllerTest.m
//  TodoList
//
//  Created by AlexDenisov on 24/09/15.
//
//

#import <XCTest/XCTest.h>
#import "TLAppController.h"
#import "TLHomeScreenViewController.h"

@interface TLAppControllerTest : XCTestCase

@property (nonatomic, strong) TLAppController *subject;
@property (nonatomic, strong) UIWindow *mockWindow;

@end

@implementation TLAppControllerTest

- (void)setUp {
    [super setUp];
    self.mockWindow = [UIWindow new];
    self.subject = [[TLAppController alloc] initWithMainWindow:self.mockWindow];
}

- (void)test_MainWindow_HasRootViewController {
    [self.subject showHomeScreen];
    XCTAssertNotNil(self.mockWindow.rootViewController, @"Sets app window root view controller");
}

- (void)test_MainWindow_HasNavigationController_AsRootViewController {
    [self.subject showHomeScreen];
    XCTAssertTrue([self.mockWindow.rootViewController isKindOfClass:[UINavigationController class]], @"MainWindow has NavigationController as a rootViewController");
}

- (void)test_MainWindow_HasNavigationController_WithHomeScreenViewController {
    [self.subject showHomeScreen];
    UINavigationController *navigationController = (UINavigationController *)self.mockWindow.rootViewController;
    UIViewController *viewController = navigationController.childViewControllers.firstObject;
    XCTAssertTrue([viewController isKindOfClass:[TLHomeScreenViewController class]], @"MainWindow shows HomeScreen inside of NavigationController");
}

@end
