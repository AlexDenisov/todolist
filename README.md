## Todolist

Simple TodoList app

### Third-part resources:

Icons: 

http://iconstore.co/icons/free-flat-icons/

Color scheme:

http://www.colourlovers.com/palette/148712/Gamebookers

### License

This porject licensed under MIT license (excerpt of third-party resources mentioned above).

See `LICENSE` for details.
